import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import RaisedButton from 'material-ui/RaisedButton';
import DatePicker from 'material-ui/DatePicker';
import TimePicker from 'material-ui/TimePicker';
import moment from 'moment';

import {
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';
import TextField from 'material-ui/TextField';

import '../Style/Tracker';
import '../Style/TimeEntry';

@inject('app') @observer
export default class TimeEntry extends Component {
    constructor(props) {
        super(props);
        this.refDict = {};
        this.app = props.app;
    }

    showTimePicker = (date, isStartDate) => {
        this.tempDate = date;
        this.isStartDate = isStartDate;
        if (this.isStartDate) {
            this.refDict[0].openDialog();
        } else {
            this.refDict[1].openDialog();
        }
    }

    handleDescriptionChange = (value) => {
        this.props.task.description = value;
    }

    handleTimeChange = (time) => {
        const { task } = this.props;
        const newDate = moment(this.tempDate);
        newDate.add(moment(time).hour(), 'hours');
        newDate.add(moment(time).minutes(), 'minutes');

        if (this.isStartDate) {
            if (newDate.isAfter(task.endDate)) {
                this.props.openAlert('New start date cannot be after end date.');
            } else {
                task.startDate = newDate;
            }
        } else if (newDate.isBefore(task.startDate)) {
            this.props.openAlert('New end date cannot be before start date.');
        } else {
            task.endDate = newDate;
        }

        this.tempDate = {};
    }

    handleRemovingItem = (task) => {
        this.app.removeEntry(task);
    }

    render() {
        const { task } = this.props;

        return (
            <TableRow key={task.id}>
                <TableRowColumn>
                    <TextField
                        name={task.id}
                        value={task.description}
                        fullWidth
                        onChange={(e, value) => this.handleDescriptionChange(value)}
                        hintText="Your task description"
                    />
                    <div className="dates">
                        <DatePicker
                            value={task.startDate.toDate()}
                            className="start-time"
                            inputStyle={{ fontSize: 12 }}
                            floatingLabelText="Start date"
                            formatDate={() => task.startDate.format('DD-MM-YYYY HH:mm:ss')}
                            onChange={(event, date) => this.showTimePicker(date, true)}
                            id={task.id}
                        />
                        <DatePicker
                            value={task.endDate.toDate()}
                            className="start-time"
                            inputStyle={{ fontSize: 12 }}
                            floatingLabelText="End date"
                            formatDate={() => task.endDate.format('DD-MM-YYYY HH:mm:ss')}
                            onChange={(event, date) => this.showTimePicker(date, false)}
                            id={task.id}
                        />
                        <TimePicker
                            format="24hr"
                            ref={(ref) => { this.refDict[0] = ref; }}
                            style={{ display: 'none' }}
                            onChange={(event, time) => this.handleTimeChange(time)}
                            id={task.id}
                        />
                        <TimePicker
                            format="24hr"
                            ref={(ref) => { this.refDict[1] = ref; }}
                            style={{ display: 'none' }}
                            onChange={(event, time) => this.handleTimeChange(time)}
                            id={task.id}
                        />
                    </div>
                </TableRowColumn>
                <TableRowColumn className="column-style">
                    {task.duration}
                </TableRowColumn>
                <TableRowColumn className="column-style">
                    <RaisedButton
                        fullWidth
                        primary
                        label="X"
                        onClick={() => this.handleRemovingItem(task)} />
                </TableRowColumn>
            </TableRow>
        );
    }
}
