import { observer, inject } from 'mobx-react';
import { observable } from 'mobx';
import React, { Component, PropTypes } from 'react';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

import { Link } from 'react-router';
import '../Style/Index';

@inject('app') @observer
export default class Index extends Component {
    static propTypes = {
        children: PropTypes.element,
    };

    static contextTypes = {
        router: PropTypes.object.isRequired,
    };

    constructor(props) {
        super(props);
        const { app } = this.props;
        this.app = app;
    }

    userLogIn = () => {
        this.app.logIn();
        if (!this.app.loggedIn) {
            this.props.router.push('/home');
        }
    }

    app = {};

    timeTrackingPanel = timer =>
        <div id="time-tracking-panel">
            <div id="tracking-button-in-panel">
                <RaisedButton
                    label={!this.app.isTracking ? 'START' : 'STOP'}
                    onClick={() => this.app.trackTime()}
                />
            </div>
            <div id="task-description-in-panel">
                <TextField
                    hintText="Your task description here"
                    fullWidth
                    onChange={(e, value) => { this.app.taskDescription = value; }}
                    value={this.app.taskDescription}
                />
            </div>
            <div id="task-time-in-panel">
                <p>{timer}</p>
            </div>
        </div>;

    render() {
        return (
            <div className="site-content">
                <header>
                    {this.app.loggedIn && this.timeTrackingPanel(this.app.timer)}
                    <div id="menu-panel">
                        <Link to="/home" >
                            <FlatButton label="Home" />
                        </Link>
                        {
                            this.app.loggedIn && <Link to="/tracker" >
                                                    <FlatButton label="Tracker" />
                                                </Link>
                        }
                        <FlatButton
                            label={this.app.loggedIn ? 'Log out' : 'Log In'}
                            onClick={() => this.userLogIn()}
                        />
                    </div>
                </header>
                <main>
                    {this.props.children}
                </main>
                <footer>Copyright all right.</footer>
            </div>
        );
    }
}

