import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { observable, computed } from 'mobx';
import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
} from 'material-ui/Table';
import Snackbar from 'material-ui/Snackbar';
import TimeEntry from './TimeEntry';
import '../Style/Tracker';

@inject('app') @observer
export default class Tracker extends Component {
    constructor(props) {
        super(props);
        this.refDict = {};
    }

    state = {
        fixedHeader: true,
        fixedFooter: true,
        stripedRows: true,
        showRowHover: false,
        selectable: true,
        multiSelectable: false,
        enableSelectAll: false,
        deselectOnClickaway: true,
        showCheckboxes: false,
    };

    @observable isOpen = false;
    @observable snackMessage = '';

    openAlert = (message) => {
        this.snackMessage = message;
        this.isOpen = true;
    }

    render() {
        const listItems = this.props.app.sortedTasks.map((task, index) =>
            <TimeEntry task={task} key={index} openAlert={(message) => this.openAlert(message)} />);

        return (
            <div className="table-wrapper">
                <Table
                    height={this.state.height}
                    fixedHeader={this.state.fixedHeader}
                    fixedFooter={this.state.fixedFooter}
                    selectable={this.state.selectable}
                    multiSelectable={this.state.multiSelectable}
                >
                    <TableHeader
                        displaySelectAll={this.state.showCheckboxes}
                        adjustForCheckbox={this.state.showCheckboxes}
                        enableSelectAll={this.state.enableSelectAll}
                    >
                        <TableRow>
                            <TableHeaderColumn tooltip="Task description, start and end date">Task</TableHeaderColumn>
                            <TableHeaderColumn className="column-style" tooltip="Duration">Duration</TableHeaderColumn>
                            <TableHeaderColumn className="column-style" tooltip="Delete entry">Delete entry</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody
                        displayRowCheckbox={this.state.showCheckboxes}
                        deselectOnClickaway={this.state.deselectOnClickaway}
                        showRowHover={this.state.showRowHover}
                        stripedRows={this.state.stripedRows}
                    >
                        {listItems}
                    </TableBody>
                </Table>
                <Snackbar
                    open={this.isOpen}
                    message={this.snackMessage}
                    autoHideDuration={4000}
                />
            </div>
        );
    }
}
