import React, { Component } from 'react';
import '../Style/Home';

export default class Home extends Component {
    render() {
        const loremIpsum = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';

        return (
            <div>
                <div className="hero">
                    <div className="center-content">
                        <h1>TIME TRACKER MACHINE</h1>
                        <h3>The most powerful app ever created!</h3>
                    </div>
                </div>
                <div className="column-content">
                    <div className="col">{loremIpsum}</div>
                    <div className="col">{loremIpsum}</div>
                    <div className="col">{loremIpsum}</div>
                </div>
            </div>
        );
    }
}
