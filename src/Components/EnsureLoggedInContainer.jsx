import React from 'react';
import { inject } from 'mobx-react';
import { observable } from 'mobx';

@inject('app')
export default class EnsureLoggedInContainer extends React.Component {
    constructor(props) {
        super(props);
        this.app = props.app;
    }

    componentDidMount() {
        if (!this.app.loggedIn) {
            this.props.router.push('/home');
        }
    }

    render() {
        if (this.app.loggedIn) {
            return this.props.children;
        }
        return null;
    }
}
