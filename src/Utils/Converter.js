import _ from 'lodash';

const convertToTime = (ms) => {
    let seconds = ((ms / 1000) % 60);
    let minutes = ((ms / (1000 * 60)) % 60);
    let hours = ((ms / (1000 * 60 * 60)) % 24);
    let days = (ms / (1000 * 60 * 60 * 24));

    seconds = Math.floor(seconds);
    minutes = Math.floor(minutes);
    hours = Math.floor(hours);
    days = Math.floor(days);

    if (days > 0) {
        return `${days}:${_.padStart(hours, 2, '0')}:${
            _.padStart(minutes, 2, '0')}:${
            _.padStart(seconds, 2, '0')}`;
    }


    return `${_.padStart(hours, 2, '0')}:${
        _.padStart(minutes, 2, '0')}:${
        _.padStart(seconds, 2, '0')}`;
};

export default convertToTime;
