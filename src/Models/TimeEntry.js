import { observable, computed } from 'mobx';
import { persist } from 'mobx-persist';
import { serializable } from 'serializr';
import convertToTime from '../Utils/Converter';

export default class TimeEntry {
    @persist @observable @serializable id = 0;
    @persist @observable @serializable description = '';
    @persist @observable @serializable startDate = {};
    @persist @observable @serializable endDate = {};

    @computed get duration() {
        if (this.startDate && this.endDate) {
            return convertToTime(this.endDate.diff(this.startDate));
        }

        return 0;
    }
}
