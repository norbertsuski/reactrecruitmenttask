import React from 'react';
import { render } from 'react-dom';
import { Router, browserHistory } from 'react-router';
import { Provider } from 'mobx-react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { appStore } from './Stores/AppStore';
import Routes from './Router/Index';

const stores = {
    app: appStore,
};

render(
    <MuiThemeProvider>
        <Provider {...stores}>
            <Router history={browserHistory} routes={Routes.routes()} />
        </Provider>
    </MuiThemeProvider>,
    document.getElementById('app'),
);
