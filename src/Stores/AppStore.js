import { observable, action, computed, intercept } from 'mobx';
import { create, persist } from 'mobx-persist';
import localforage from 'localforage';
import Moment from 'moment';
import Guid from 'guid';
import _ from 'lodash';
import { extendMoment } from 'moment-range';
import TimeEntry from '../Models/TimeEntry';
import convertToTime from '../Utils/Converter';

const moment = extendMoment(Moment);

class AppStore {
    constructor() {
        localforage.config({
            driver: localforage.WEBSQL, // Force WebSQL; same as using setDriver()
            name: 'timeTrackerMachine',
            version: 1.0,
            size: 4980736, // Size of database, in bytes. WebSQL-only for now.
            storeName: 'trackerStore', // Should be alphanumeric, with underscores.
            description: 'My store',
        });

        localforage.getItem('loggedIn')
            .then((value) => { this.loggedIn = value; })
            .catch(err => console.log(err));

        localforage.getItem('tasks')
            .then((value) => {
                if (value) {
                    this.tasks = value.map(task => {
                        const timeEntry = new TimeEntry();
                        timeEntry.id = task.id;
                        timeEntry.description = task.description;
                        timeEntry.startDate = moment(task.startDate);
                        timeEntry.endDate = moment(task.endDate);

                        return timeEntry;
                    });
                }
            })
            .catch((err) => {
                console.log(`loading tasks error: ${err}`);
                this.tasks = [];
            });

        localforage.getItem('startDate')
            .then((value) => { this.startDate = value; })
            .catch(err => console.log(err));

        if (this.loggedIn && this.startDate) {
            this.trackTime();
        }
    }

    @observable @persist loggedIn = false;
    @observable @persist('list', TimeEntry) tasks = [];
    @observable isTracking = false;
    @observable timer = '00:00:00';
    @observable counter = 0;
    @observable taskDescription = '';
    startDate = {};

    @action trackTime = () => {
        if (!this.isTracking) {
            this.startDate = moment();
            this.counter = setInterval(() => this.tick(), 1000);
        } else {
            const timeEntry = new TimeEntry();
            timeEntry.id = Guid.raw();
            timeEntry.startDate = this.startDate;
            timeEntry.endDate = moment();
            timeEntry.description = this.taskDescription;
            this.tasks.push(timeEntry);

            this.timer = '00:00:00';

            this.startDate = {};
            this.taskDescription = '';
            clearInterval(this.counter);

            this.saveTasks();
        }

        this.isTracking = !this.isTracking;
    }

    @computed get sortedTasks() {
        return this.tasks.sort((a, b) => (a.endDate.isBefore(b.endDate)));
    }

    @action removeEntry = (task) => {
        _.pull(this.tasks, task);
        this.saveTasks();
    }

    @action saveTasks = () => {
        localforage.setItem('tasks', this.tasks)
            .then(() => localforage.getItem('tasks'))
            .then(value => console.log(value))
            .catch(err => console.log(err));
    }

    @action logIn = () => {
        localforage.setItem('loggedIn', !this.loggedIn)
            .then(() => localforage.getItem('loggedIn'))
            .then(value => console.log(value))
            .catch(err => console.log(err));
        this.loggedIn = !this.loggedIn;
    }

    @action tick = () => {
        const diff = moment().diff(this.startDate);
        this.timer = convertToTime(diff);
    }
}


const hydrate = create({
    default: localforage,
    jsonify: false,
});

export const appStore = new AppStore();

hydrate('some', appStore).then(() => console.log('hydrated'));
