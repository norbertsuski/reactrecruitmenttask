export const index = (location, callback) => {
    require.ensure([], (require) => {
        callback(null, require('../Components/Index').default);
    }, 'index');
};

export const home = (location, callback) => {
    require.ensure([], (require) => {
        callback(null, require('../Components/Home').default);
    }, 'home');
};

export const tracker = (location, callback) => {
    require.ensure([], (require) => {
        callback(null, require('../Components/Tracker').default);
    }, 'tracker');
};
