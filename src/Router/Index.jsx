import React from 'react';
import { Route, IndexRoute } from 'react-router';
import EnsureLoggedInContainer from '../Components/EnsureLoggedInContainer';

import {
    index,
    home,
    tracker,
} from './Chunks';

const routes = params => (
        <Route path="/" getComponent={index}>
            <IndexRoute getComponent={home} />
            <Route path="/home" getComponent={home} params={params} />
            <Route component={EnsureLoggedInContainer}>
                <Route path="/tracker" getComponent={tracker} params={params} />
            </Route>
        </Route>
    );

export default { routes };

